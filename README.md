# map-links

List of web mapping platform URLs for certain places.

## Places

### Lisbon

```plain
https://www.openstreetmap.org/?mlat=38.7077507&mlon=-9.1365919
```

```plain
https://wego.here.com/?map=38.7077507,-9.1365919,12
```

## Development

```bash
pyenv install && pyenv versions
```

```bash
pip install pipenv==2023.11.15 && pipenv --version
```

```bash
pipenv install --dev --skip-lock
```

```bash
pipenv run codespell
```

```bash
pipenv run ruff check --verbose .
```

```bash
pipenv run ruff check --fix --verbose . && pipenv run ruff format --verbose .
```

```bash
pipenv run python 01.py
```
