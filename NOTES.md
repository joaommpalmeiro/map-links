# Notes

- https://en.wikipedia.org/wiki/Web_mapping
- https://en.wikipedia.org/wiki/Google_Maps
- https://pypi.org/project/requests-cache/
- https://github.com/requests-cache/requests-cache
- https://wiki.openstreetmap.org/wiki/Browsing#Other_URL_tricks
  - "The parameters _mlat_ and _mlon_ are used to indicate the position of the red marker."
- https://nominatim.org/release-docs/latest/api/Overview/
- https://nominatim.org/release-docs/latest/api/Search/
- https://requests-cache.readthedocs.io/en/stable/user_guide/expiration.html
- https://requests.readthedocs.io/en/latest/user/advanced/
- https://requests-cache.readthedocs.io/en/stable/modules/requests_cache.session.html#requests_cache.session.CachedSession.get
- https://requests.readthedocs.io/en/latest/api/#requests.get
- https://requests.readthedocs.io/en/latest/api/#requests.request
- https://requests-cache.readthedocs.io/en/stable/modules/requests_cache.models.response.html#requests_cache.models.response.BaseResponse.from_cache
- https://requests.readthedocs.io/en/latest/api/#lower-lower-level-classes
- https://wiki.openstreetmap.org/wiki/Layer_URL_parameter
- HERE WeGo:
  - https://www.here.com/docs/
  - https://developer.here.com/documentation/deeplink-web/dev_guide/topics/request-format.html
  - https://developer.here.com/documentation/deeplink-web/dev_guide/topics/share-location.html
  - https://developer.here.com/documentation/deeplink-web/dev_guide/topics/share-place.html
  - https://www.here.com/docs/bundle/places-search-api-developer-guide/page/topics/what-is.html
  - https://www.here.com/docs/bundle/places-search-api-developer-guide/page/topics/quick-start-find-text-string.html
  - https://www.here.com/docs/bundle/identity-and-access-management-developer-guide/page/topics/dev-apikey.html
  - https://developer.here.com/
  - https://developer.here.com/develop/rest-apis
  - https://www.here.com/learn/blog?type=developer

## Commands

```bash
pipenv install --skip-lock requests-cache==1.1.1 && pipenv install --dev --skip-lock ruff==0.1.11 codespell==2.2.6
```

```bash
pipenv --rm
```

```bash
pipenv run codespell --help
```
