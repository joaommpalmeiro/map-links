from datetime import timedelta
from urllib.parse import urlencode

import requests_cache

DEFAULT_ZOOM = 12

SEARCH_BASE_URL = "https://nominatim.openstreetmap.org/search"

OSM_BASE_URL = "https://www.openstreetmap.org/"
HERE_BASE_URL = "https://wego.here.com/"

if __name__ == "__main__":
    with requests_cache.CachedSession(
        "map_links_cache", expire_after=timedelta(days=30)
    ) as s:
        r = s.get(
            SEARCH_BASE_URL,
            params={"q": "Lisbon", "format": "jsonv2", "limit": 1},
        )
        data = r.json()[0]

        print(r.url, r.from_cache)
        print(data["display_name"])

        osm_params = urlencode({"mlat": data["lat"], "mlon": data["lon"]})
        osm_url = f"{OSM_BASE_URL}?{osm_params}"
        print(osm_url)

        here_params = urlencode(
            {"map": f"{data['lat']},{data['lon']},{DEFAULT_ZOOM}"}, safe=","
        )
        here_url = f"{HERE_BASE_URL}?{here_params}"
        print(here_url)
